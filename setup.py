import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="example-pkg-YOUR-USERNAME-HERE", # Replace with your own username
    version="0.0.1",
    author="Barry Muldrey",
    author_email="barry@muldrey.net",
    description="Grabs system and author info and signs and encrypts directory for student submission",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/bjmuld/ssubmit",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)"
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=['pyarmor'],
)
