#!/usr/bin/env python3
import subprocess
import shlex
import time
import datetime
import hashlib
import uuid
import argparse
import base64
import os
import yaml
import shutil
import stat
import re

from subprocess import CalledProcessError

from pathlib import Path
from cryptography.fernet import Fernet
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.backends import default_backend
from py_compile import compile

commands = [
    'hash'
    'package'
    'sign'
    'decrypt'
]

BK_RE = r'(?<=BUNDLED_KEY = b\')\S+(?=\')'
BUNDLED_KEY = b'yJWWoyA5aG98kxqNjLxzYTHUOzXiOqdZN5waPxX8nOg='
BUNDLE_NAME = 'bundle'
BUNDLE_EXE = 'ssubmit.pyo'

THIS_DIR = Path(__file__).parent
KEY_FILE = THIS_DIR / 'ssubmit.key'
BUNDLE_FILE = Path(__file__).with_name(BUNDLE_NAME)
TEMP_FILE = THIS_DIR / 'tempfile.py'


def edie(message, exit_code=1):
    print(message)
    exit(exit_code)


def cmd_output(cmd):
    c = shlex.split('bash -c \'{} 2>&1 \''.format(cmd))
    try:
        s = subprocess.check_output(c).decode()
    except CalledProcessError:
        edie('COMMAND FAILED: {}'.format(cmd))
    return s


def cmd_capture(cmd):
    c = shlex.split('bash -c \'{} 2>&1 \''.format(cmd))
    try:
        s = subprocess.run(c, check=True, capture_output=True)
        return s

    except CalledProcessError as e:
        for line in e.stdout.decode().split('\n'):
            l = line.strip()
            if not l: continue
            print('{} [STDOUT]: {}'.format(cmd, l))
        for line in e.stderr.decode().split('\n'):
            l = line.strip()
            if not l: continue
            print('{} [STDERR]: {}'.format(cmd, l))


def make_key():
    kdf = PBKDF2HMAC(
        backend=default_backend(),
        algorithm=hashes.SHA256(),
        length=32,
        salt=uuid.uuid4().hex.encode(),
        iterations=100000,
    )
    return base64.urlsafe_b64encode(kdf.derive(uuid.uuid4().hex.encode()))


def make_pathtree(path, node=None):
    # tt = cmd_output('find {} -type f -print0 | xargs -0 ls -l --time-style="+%F %T"'.format(path))
    tt = cmd_output('tree -aD')
    return tt


def build_info():
    info = {
        'hostname': cmd_output('hostname').strip(),
        'username': cmd_output('whoami').strip(),
        'datetime': datetime.datetime.now(),
        'interfaces': cmd_output('ip link'),
        'tree': make_pathtree(Path(__file__).parent)
    }

    # test = yaml.load(textblock)
    return yaml.dump(info)


def make_sig(key):
    text = build_info()
    f = Fernet(key)
    token = f.encrypt(text.encode())
    return token


def git_commit():
    cmd_capture('git add -A')
    cmd_capture('git commit -am \"automated commit by SSUBMIT.py\"')


def git_push():
    cmd_capture('git push')


def write_key(key):
    with open(KEY_FILE, 'wb') as f:
        f.write(key)
    print('key written to {}'.format(KEY_FILE))


def read_key():
    with open(KEY_FILE, 'rb') as f:
        key = f.read()
    return key


def loadmake_key():
    if not KEY_FILE.is_file():
        key = make_key()
        write_key(key)
    else:
        key = read_key()
    return key


# def pyc_obfuscate():
#     compile(__file__, cfile=BUNDLE_FILE)
#     # with open(target_file, 'w+') as f:
#     #     f.seek(0)
#     #     f.write('#!/usr/bin/env python3\n')
#     os.chmod(BUNDLE_FILE,
#              mode=stat.S_IRUSR | stat.S_IRGRP | stat.S_IROTH | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)
#     print('wrote obfuscated package to {} with key {}'.format(BUNDLE_FILE, key))


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument('command', action='store', help='top-level command. Available commands:{}'.format(commands))
    args, uargs = ap.parse_known_args()

    if args.command == 'key':
        # hp = argparse.ArgumentParser(add_help=False, parents=[ap])
        # hp.add_argument('password', help='password to salt and hash')
        # args = hp.parse_args()

        key = make_key()
        write_key(key)

    elif args.command == 'bundle':
        # bp = argparse.ArgumentParser(add_help=False, parents=[ap])
        # bp.add_argument('password', help='password to salt and hash')
        # args = bp.parse_args()

        key = loadmake_key()

        with open(__file__, 'r+') as f:
            cc = f.read()
            cc = re.sub(BK_RE, re.escape(key.decode()), cc, re.MULTILINE)
            f.seek(0)
            f.write(cc)
            f.truncate()

        cmd_capture('pyarmor obfuscate {}'.format(__file__))
        os.rename(THIS_DIR / 'dist' / Path(__file__).name, THIS_DIR / 'dist' / Path(__file__).stem)
        os.chmod(THIS_DIR / 'dist' / Path(__file__).stem, 0o555)

    elif args.command == 'test':
        # tp = argparse.ArgumentParser(add_help=False, parents=[ap])
        # tp.add_argument('password', help='password to salt and hash')
        # args = tp.parse_args()

        key = loadmake_key()
        token = make_sig(key)
        f = Fernet(key)
        decrypted = f.decrypt(token)
        print('key: {}'.format(key))
        print('crypttext: {}'.format(token.decode()))
        print(decrypted.decode())

    elif args.command == 'sign':
        try:
            assert 'BUNDLED_KEY' in globals() and BUNDLED_KEY
        except AssertionError:
            edie("no key has been bundled with this script", 1)

        token = make_sig(BUNDLED_KEY)
        with open('SSUBMIT.sig', 'w') as f:
            f.write(token.decode())

        git_commit()
        git_push()

    elif args.command == 'decode':
        tp = argparse.ArgumentParser(add_help=False, parents=[ap])
        tp.add_argument('signature_file', action='store', type=str, help='path to signature file to decode')
        args = tp.parse_args()

        key = loadmake_key()
        with open(Path(args.signature_file).expanduser(), 'rb') as f:
            token = f.read()
        f = Fernet(key)
        decrypted = yaml.safe_load(f.decrypt(token))
        for k in sorted(decrypted):
            print(str(k) + ':')
            for line in str(decrypted[k]).split('\n'):
                l = line.strip()
                if l:
                    print('    ' + l)

    else:
        ap.print_help()
        edie('')


if __name__ == '__main__':
    main()
